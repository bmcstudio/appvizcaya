(function() {

    'use strict';

    angular.module('AppVizcaya.services', []);
    angular.module('AppVizcaya.directives', []);
    angular.module('AppVizcaya.controllers', []);

    // app Vizcaya
    // by: Douglas de Oliveira

    angular.module('AppVizcaya', ['ui.router', 'ngSanitize', 'ngTouch', 'Angular.webSyncSQL', 'angularRipple', 'AppVizcaya.config', 'AppVizcaya.httpInterceptor', 'AppVizcaya.providers', 'AppVizcaya.services', 'AppVizcaya.directives', 'AppVizcaya.controllers'])

    .run(function($window, $cordovaService, $database, $rootScope) {

        if (typeof(navigator.notification) == "undefined") {
            navigator.notification = window;
        };

        // loading
        $rootScope.isLoading = true;

        // init Fastclick
        FastClick.attach(angular.element($window.document.body)[0]);

        // when cordova is ready
        // init local storage
        $cordovaService.ready().then(
            function resolved(resp) {
                $database.syncNow();
            },
            function rejected(resp){
                $database.syncNow();
            }
        );
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/base.html'
            })

            .state('app.login', {
                url: '/login',
                views:{
                    'mainContent':{
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl as vm'
                    }
                }
            })

            .state('app.home', {
                url: '/home',
                views:{
                    'mainContent':{
                        templateUrl: 'templates/home.html',
                        controller: 'homeCtrl as vm'
                    }
                }
            })

            .state('app.cadastrouser', {
                url: '/cadastro-user',
                views:{
                    'mainContent':{
                        templateUrl: 'templates/cadastro-user.html',
                        controller: 'cadastroUserCtrl as vm'
                    }
                }
            })

            .state('app.diagnostico', {
                url: '/diagnostico',
                views:{
                    'mainContent':{
                        templateUrl: 'templates/diagnostico.html',
                        controller: 'diagnosticoCtrl as vm'
                    }
                }
            })

            .state('app.cadastrovenda', {
                url: '/cadastro-venda',
                views:{
                    'mainContent':{
                        templateUrl: 'templates/cadastro-venda.html',
                        controller: 'cadastroVendaCtrl as vm'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/login');

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');
    });

})();