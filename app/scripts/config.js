(function() {

	'use strict';

	angular.module('AppVizcaya.config', [])
		.constant('DB_CONFIG', {
			dbName: 'mydb',
			sync_URL: 'http://bmc.ppg.br/bardez/Vizcaya/webSqlSyncAdapter.php',
			isDebug: false,
			// sync_URL: 'http://www.vizcaya.com.br/AppWeb/siteadmin/WebSync/webSqlSyncAdapter.php',
			sync_info: { //Example of user info
				userEmail         : 'name@abc.com', //the user mail is not always here
				device_uuid       : 'UNIQUE_DEVICE_ID_287CHBE873JB', //if no user mail, rely on the UUID
				lastSyncDate      : '0',
				device_version    : navigator.appVersion,
				device_name       : 'Code name: ' + navigator.appCodeName + '/ App name: ' + navigator.appName,
				userAgent         : navigator.userAgent,
				//app data
				appName           : 'VizcayaApp',
				webSqlApp_version : '1.0',
				lng               : 'br'
			},
			tablesToSync: [{
				tableName : 'admin_user',
				idName    : 'adminID'
			}, {
				tableName : 'clientes',
				idName    : 'userID'
			}, {
				tableName : 'pdv'
			}],
			tables: [{
				name: 'admin_user',
				columns: {
					'adminID': {
						'type': 'INTEGER',
						'null': 'NOT NULL', // default is 'NULL' (if not defined)
						'primary': true, // primary
						'auto_increment': true // auto increment
					},
					'created': {
						'type': 'DATETIME',
						'null': 'NOT NULL'
					},
					'username': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'password': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'email': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'fone': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'name': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'type': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					'diags': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					'last_sync_date': {
						'type': 'TIMESTAMP',
						'null': 'NOT NULL'
					}
				}
			}, {
				name: 'clientes',
				columns: {
					'userID': {
						'type': 'INTEGER',
						'null': 'NOT NULL', // default is 'NULL' (if not defined)
						'primary': true, // primary
						'auto_increment': true // auto increment
					},
					'created': {
						'type': 'DATETIME',
						'null': 'NOT NULL'
					},
					'email': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'img': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'name': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'consultorID': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't1normal': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't1oleoso': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't1misto': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't1maduros': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't2liso': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't2ondulado': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't2cacheado': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't2crespo': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't3preto': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't3castanho': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't3loiro': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't3branco': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't3ruivo': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4ressecados': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4quebradicos': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4danificados': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4queda': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4frizz': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4volume': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4finos': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't4residuos': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't5alisamento': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't5nenhum': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't5coloracao': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					't5descoloracao': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
				 	'newsletter': {
						'type': 'INTEGER',
						'null': 'NOT NULL'
					},
					'last_sync_date': {
						'type': 'TIMESTAMP',
						'null': 'NOT NULL'
					}
				}
			}, {
				name: 'pdv',
				columns: {
					'id': {
						'type': 'INTEGER',
						'null': 'NOT NULL', // default is 'NULL' (if not defined)
						'primary': true, // primary
						'auto_increment': true // auto increment
					},
					'created': {
						'type': 'DATETIME',
						'null': 'NOT NULL'
					},
					'nome': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'endereco': {
						'type': 'VARCHAR',
						'null': 'NOT NULL'
					},
					'img0': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'img1': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'img2': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'img3': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'img4': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'img5': {
						'type': 'MEDIUMTEXT',
						'null': 'NOT NULL'
					},
					'last_sync_date': {
						'type': 'TIMESTAMP',
						'null': 'NOT NULL'
					}
				}
			}]
		});

})();