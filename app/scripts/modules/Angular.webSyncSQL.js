(function() {

	'use strict';

	// developer by: Douglas de Oliveira
	// reference link:
	//  - https://github.com/abeauseigle/webSqlApp
	//  - https://github.com/paulocaldeira17/angular-websql

	angular.module('Angular.webSyncSQL', [])
		.factory('$webSyncSQL', webSyncSQL);

	webSyncSQL.$inject = ['$http', '$q'];

	function webSyncSQL($http, $q) {

		var DBSYNC = {
			db           : null,
			serverUrl    : null,
			isDebug      : false,
			tablesToSync : [], 			// eg.  [{tableName : 'myDbTable', idName : 'myTable_id'},{tableName : 'stat'}]
			idNameFromTableName: {}, 	// map to get the idName with the tableName (key)
			syncInfo: { 				// this object can have other useful info for the server ex. {deviceId : "XXXX", email : "fake@g.com"}
				lastSyncDate: null 		// attribute managed by webSqlSync
			},
			syncResult      : null,
			firstSync       : false,
			cbEndSync       : null,
			clientData      : null,
			serverData      : null,
			openDB          : openDB,
			insert          : insert,
			update          : update,
			del             : del,
			select          : select,
			selectAll       : selectAll,
			createTable     : createTable,
			dropTable       : dropTable,
			initSync        : initSync,
			syncNow         : syncNow,
			getLastSyncDate : getLastSyncDate,
			setSyncDate     : setSyncDate,
			setFirstSync    : setFirstSync
		};

		var fnPrivate = {
			log                      : log,
			error                    : error,
			checkIsOnline            : checkIsOnline,
			getDataToBackup          : getDataToBackup,
			getDataToSave            : getDataToSave,
			sendDataToServer         : sendDataToServer,
			updateLocalDb            : updateLocalDb,
			getIdExitingInDB         : getIdExitingInDB,
			finishSync               : finishSync,
			whereClause              : whereClause,
			executeQuery             : executeQuery,
			replace                  : replace,
			selectSql                : selectSql,
			transformRs              : transformRs,
			errorHandler             : errorHandler,
			buildInsertSQL           : buildInsertSQL,
			buildInsertSQLWithIdNull : buildInsertSQLWithIdNull,
			getMembersValueForIdNull : getMembersValueForIdNull,
			buildUpdateSQL           : buildUpdateSQL,
			getMembersValue          : getMembersValue,
			getAttributesList        : getAttributesList,
			getNbValString           : getNbValString,
			getMembersValueString    : getMembersValueString,
			arrayToString            : arrayToString
		};

		return DBSYNC;

		///////////////////////////////////////////

		/*************** PUBLIC FUNCTIONS ********************/
		/**
		 * Open database
		 * @param {String} dbName : name WebSQL database object.
		 * @param {String} version : version WebSQL database object.
		 * @param {String} desc : description of database
		 * @param {Int} size : size WebSQL database object.
		 */
		function openDB (dbName, version, desc, size) {
			DBSYNC.db = openDatabase(dbName, version, desc, size);

			if (typeof(openDatabase) == "undefined")
				throw "Browser does not support web sql";
		}

		/**
		 * @param {String} c : name of table.
		 * @param {Object} e : params to insert into the table.
		 * @param {boolean} r : set if INSERT OR REPLACE or only INSERT.
		 */
		function insert(c, e, r) {
			var f = (typeof r === "boolean" && r) ? "INSERT OR REPLACE" : "INSERT";
			f += " INTO `{tableName}` ({fields}) VALUES({values});";
			var a = "",
				b = "",
				v = [];
			for (var d in e) {
				a += (Object.keys(e)[Object.keys(e).length - 1] == d) ? "`" + d + "`" : "`" + d + "`, ";
				b += (Object.keys(e)[Object.keys(e).length - 1] == d) ? "?" : "?, ";
				v.push(e[d]);
			}
			return fnPrivate.executeQuery(fnPrivate.replace(f, {
				"{tableName}": c,
				"{fields}": a,
				"{values}": b
			}), v);
		}

		/**
		 * @param {String} b : name of table.
		 * @param {Object} g : params to update into the table.
		 * @param {boolean} c : set if INSERT OR REPLACE or only INSERT.
		 */
		function update(b, g, c) {
			var f = "UPDATE `{tableName}` SET {update} WHERE {where}; ";
			var e = "";
			var v = [];
			for (var d in g) {
				e += (Object.keys(g)[Object.keys(g).length - 1] == d) ? "`" + d + "`= ?" : "`" + d + "`= ?,";
				v.push(g[d]);
			}
			var a = fnPrivate.whereClause(c);
			return fnPrivate.executeQuery(fnPrivate.replace(f, {
				"{tableName}": b,
				"{update}": e,
				"{where}": a.w
			}), v.concat(a.p));
		}

		/**
		 * @param {String} b : name of table.
		 * @param {Object} c : params to delete into the table.
		 */
		function del(b, c) {
			var d = "DELETE FROM `{tableName}` WHERE {where}; ";
			var a = fnPrivate.whereClause(c);
			return fnPrivate.executeQuery(fnPrivate.replace(d, {
				"{tableName}": b,
				"{where}": a.w
			}), a.p);
		}

		/**
		 * @param {String} b : name of table.
		 * @param {Object} c : params to select into the table.
		 */
		function select(b, c) {
			var d = "SELECT * FROM `{tableName}` WHERE {where}; ";
			var a = fnPrivate.whereClause(c);
			return fnPrivate.executeQuery(fnPrivate.replace(d, {
				"{tableName}": b,
				"{where}": a.w
			}), a.p);
		}

		/**
		 * @param {String} a : name of table.
		 */
		function selectAll(a) {
			return fnPrivate.executeQuery("SELECT * FROM `" + a + "`; ", []);
		}

		/**
		 * @param {String} j : name of table.
		 * @param {Object} g : params to create the columns into the table.
		 */
		function createTable(j, g) {
			var b = "CREATE TABLE IF NOT EXISTS `{tableName}` ({fields}); ";
			var c = [];
			var a = "";
			for (var e in g) {
				var l = "{type} {null}";
				a += "`" + e + "` ";
				if (typeof g[e]["null"] === "undefined") g[e]["null"] = "NULL";
				for (var k in g[e]) {
					l = l.replace(new RegExp("{" + k + "}", "ig"), g[e][k])
				}
				a += l;
				if (typeof g[e]["default"] !== "undefined") {
					a += " DEFAULT " + g[e]["default"]
				}
				if (typeof g[e]["primary"] !== "undefined") {
					a += " PRIMARY KEY"
				}
				if (typeof g[e]["auto_increment"] !== "undefined") {
					a += " AUTOINCREMENT"
				}
				if (Object.keys(g)[Object.keys(g).length - 1] != e) {
					a += ","
				}
				if (typeof g[e]["primary"] !== "undefined" && g[e]["primary"]) {
					c.push(e)
				}
			}
			var d = {
				tableName: j,
				fields: a
			};
			for (var f in d) {
				b = b.replace(new RegExp("{" + f + "}", "ig"), d[f])
			}
			return fnPrivate.executeQuery(b, []);
		}

		/**
		 * @param {String} a : name of table.
		 */
		function dropTable(a) {
			return fnPrivate.executeQuery("DROP TABLE IF EXISTS `" + a + "`; ", []);
		}

		/**
		 * Initialize the synchronization (should be called before any call to syncNow)
		 * (it will create automatically the necessary tables and triggers if needed)
		 * @param {Object} theTablesToSync : ex : [{ tableName: 'card_stat', idName: 'card_id'}, {tableName: 'stat'}] //no need to precise id if the idName is "id".
		 * @param {Object} theSyncInfo : will be sent to the server (useful to store any ID or device info).
		 * @param {Object} theServerUrl
		 */
		function initSync(theTablesToSync, theSyncInfo, theServerUrl, isDebug) {

			DBSYNC.tablesToSync = theTablesToSync;
			DBSYNC.syncInfo     = theSyncInfo;
			DBSYNC.serverUrl    = theServerUrl;
			DBSYNC.isDebug      = isDebug || false;

			var self = DBSYNC, i = 0, deferred = $q.defer();

			//Handle optional id :
			for (i = 0; i < self.tablesToSync.length; i++) {
				if (typeof self.tablesToSync[i].idName === 'undefined') {
					self.tablesToSync[i].idName = 'id'; //if not specified, the default name is 'id'
				}
				self.idNameFromTableName[self.tablesToSync[i].tableName] = self.tablesToSync[i].idName;
			}

			self.db.transaction(function(transaction) {
				//create new table to store modified or new elems
				fnPrivate.executeQuery('CREATE TABLE IF NOT EXISTS new_elem (table_name TEXT NOT NULL, id TEXT NOT NULL);', []);
				fnPrivate.executeQuery('CREATE INDEX IF NOT EXISTS index_tableName_newElem on new_elem (table_name);', []);
				fnPrivate.executeQuery('CREATE TABLE IF NOT EXISTS sync_info (last_sync TIMESTAMP);', []);

				//create triggers to automatically fill the new_elem table (this table will contains a pointer to all the modified data)
				for (i = 0; i < self.tablesToSync.length; i++) {
					var curr = self.tablesToSync[i];
					fnPrivate.executeQuery('CREATE TRIGGER IF NOT EXISTS update_' + curr.tableName + '  AFTER UPDATE ON ' + curr.tableName + ' ' +
						'BEGIN INSERT INTO new_elem (table_name, id) VALUES ("' + curr.tableName + '", new.' + curr.idName + '); END;', []);

					fnPrivate.executeQuery('CREATE TRIGGER IF NOT EXISTS insert_' + curr.tableName + '  AFTER INSERT ON ' + curr.tableName + ' ' +
						'BEGIN INSERT INTO new_elem (table_name, id) VALUES ("' + curr.tableName + '", new.' + curr.idName + '); END;', []);
					//TODO the DELETE is not handled. But it's not a pb if you do a logic delete (ex. update set state="DELETED")
				}

				fnPrivate.selectSql('SELECT last_sync FROM sync_info').then(function(res) {

					if (res.length === 0 || res[0] == 0) { //First sync (or data lost)
						fnPrivate.executeQuery('INSERT OR REPLACE INTO sync_info (last_sync) VALUES (0)', []);
						self.firstSync = true;
						self.syncInfo.lastSyncDate = 0;
						deferred.resolve(true);
					} else {
						self.syncInfo.lastSyncDate = res[0].last_sync;
						if (self.syncInfo.lastSyncDate === 0) {
							self.firstSync = true;
						}
						deferred.resolve(false);
					}
				}, fnPrivate.errorHandler);

			}); //end tx

			return deferred.promise;
		}

		/**
		 *
		 * @param {boolean} saveBandwidth (default false): if true, the client will not send a request to the server if there is no local changes
		 */
		function syncNow(saveBandwidth) {
			var self = DBSYNC, deferred = $q.defer();

			if (!fnPrivate.checkIsOnline()) {
				deferred.reject();
				return deferred.promise;
			}

			if (self.db === null) {
				throw 'You should call the initSync before (db is null)';
			}

			self.syncResult = {
				syncOK    : false,
				codeStr   : 'noSync',
				message   : 'No Sync yet',
				nbSent    : 0,
				nbUpdated : 0
			};

			var progress = {
				message: 'Getting local data to backup',
				percent: 0,
				action: 'getData'
			};

			self.cbEndSync = function() {
				progress.message = self.syncResult.message;
				progress.percent = 100;
				progress.action = self.syncResult.codeStr;

				deferred.notify(progress);
				deferred.resolve(self.syncResult);
			};

			deferred.notify(progress);

			fnPrivate.getDataToBackup().then(function(data) {
				self.clientData = data;
				if (saveBandwidth && self.syncResult.nbSent === 0) {
					self.syncResult.localDataUpdated = false;
					self.syncResult.syncOK = true;
					self.syncResult.codeStr = 'Nothing To Send';
					self.syncResult.message = 'No new data to send to the server';
					self.cbEndSync(self.syncResult);
					return;
				}

				progress.message = 'Sending ' + self.syncResult.nbSent + ' elements to the server';
				progress.percent = 20;
				progress.action = 'senData';

				deferred.notify(progress);

				fnPrivate.sendDataToServer(data).then(function(serverData) {

					progress.message = 'Updating local data';
					progress.percent = 70;
					progress.action = 'updateData';

					deferred.notify(progress);

					fnPrivate.updateLocalDb(serverData).then(function() {
						self.syncResult.localDataUpdated = self.syncResult.nbUpdated > 0;
						self.syncResult.syncOK = true;
						self.syncResult.codeStr = 'syncOk';
						self.syncResult.message = 'Data synchronized successfuly. (' + self.syncResult.nbSent + ' new/modified element saved, ' + self.syncResult.nbUpdated + ' updated)';
						self.syncResult.serverAnswer = serverData; //include the original server answer, just in case
						//console.log("ln 154 serverData.syncDate: " + serverData.syncDate	this.syncInfo.lastSyncDate); //AB+ R: undefined
						//console.log("ln 155 serverData.syncDate: " + this.syncInfo.lastSyncDate); //AB+ R: bug -> Cannot read property 'lastSyncDate' of undefined
						self.cbEndSync(self.syncResult);
					}, fnPrivate.errorHandler);
				}, fnPrivate.errorHandler);
			}, fnPrivate.errorHandler);

			return deferred.promise;
		}

		function getLastSyncDate() {
			fnPrivate.log("DBSYNC.syncInfo.lastSyncDate" + DBSYNC.syncInfo.lastSyncDate); //AB+
			return DBSYNC.syncInfo.lastSyncDate;
		}
		// Usefull to tell the server to resend all the data from a particular Date (val = 1 : the server will send all his data)
		function setSyncDate(val) {
			DBSYNC.syncInfo.lastSyncDate = val;
			DBSYNC.executeQuery('UPDATE sync_info SET last_sync = "' + DBSYNC.syncInfo.lastSyncDate + '"', []);
		}
		//Useful to tell the client to send all his data again (like the firstSync)
		function setFirstSync() {
			DBSYNC.firstSync = true;
			DBSYNC.syncInfo.lastSyncDate = 0;
			DBSYNC.executeQuery('UPDATE sync_info SET last_sync = "' + DBSYNC.syncInfo.lastSyncDate + '"', []);
		}

		/*************** PRIVATE FUNCTIONS ********************/

		/* You can override the following methods to use your own log */
		function log(message) {
			if (DBSYNC.isDebug) console.log(message);
		}

		function error(message) {
			if (DBSYNC.isDebug) console.error(message);
		}

		function checkIsOnline() {

			// Handle IE and more capable browsers
			var xhr = new ( window.ActiveXObject || XMLHttpRequest )( "Microsoft.XMLHTTP" );
			var status;

			// Open new request as a HEAD to the root hostname with a random param to bust the cache
			xhr.open( "HEAD", DBSYNC.serverUrl + "/?rand=" + Math.floor((1 + Math.random()) * 0x10000), false );

			// Issue request and handle response
			try {
				xhr.send();
				return ( xhr.status >= 200 && xhr.status < 300 || xhr.status === 304 );
			} catch (error) {
				return false;
			}
		}

		function getDataToBackup() {
			fnPrivate.log('============ Get Data To Backup ============');

			var self = DBSYNC, nbData = 0, deferred = $q.defer(),
				dataToSync = {
					info: self.syncInfo,
					data: {}
				};

			self.db.transaction(function(tx) {
				var i, counter = 0,
					nbTables = self.tablesToSync.length,
					currTable;

				self.tablesToSync.forEach(function(currTable) { //a simple for will not work here because we have an asynchronous call inside
					fnPrivate.getDataToSave(currTable.tableName, currTable.idName, self.firstSync, tx).then(function(data) {
						dataToSync.data[currTable.tableName] = data;
						nbData += data.length;
						counter++;
						if (counter === nbTables) { //only call the callback at the last table
							fnPrivate.log('Data fetched from the local DB');
							//dataToSync.info.nbDataToBackup = nbData;
							self.syncResult.nbSent = nbData;
							deferred.resolve(dataToSync);
						}
					});
				}); //end for each
			}); //end tx
			return deferred.promise;
		}

		function getDataToSave(tableName, idName, needAllData, tx) {
			var self = DBSYNC, sql = '', deferred = $q.defer();

			if (needAllData) {
				sql = 'SELECT * FROM ' + tableName;
			} else {
				sql = 'SELECT * FROM ' + tableName + ' WHERE ' + idName + ' IN (SELECT DISTINCT id FROM new_elem WHERE table_name="' + tableName + '")';
			}
			fnPrivate.selectSql(sql).then(function(response){
				deferred.resolve(response);
			});

			return deferred.promise;
		}

		function sendDataToServer(dataToSync) {
			var self = DBSYNC, deferred = $q.defer();

			if (!fnPrivate.checkIsOnline()) {
				deferred.resolve();
				return deferred.promise;
			}

			$http({ headers: {'Content-Type': 'application/x-www-form-urlencoded'}, url: self.serverUrl, method: "POST", data: dataToSync }).then(function(response){
 				fnPrivate.log('Server answered: ');
				fnPrivate.log(response.data);
				deferred.resolve(response.data);
			});

			return deferred.promise;
		}

		function updateLocalDb(serverData) {
			var self = DBSYNC, deferred = $q.defer();
			self.serverData = serverData;

			if (!serverData || serverData.result === 'ERROR') {
				self.syncResult.syncOK = false;
				self.syncResult.codeStr = 'syncKoServer';
				if (serverData) {
					self.syncResult.message = serverData.message;
				} else {
					self.syncResult.message = 'No answer from the server';
				}
				self.cbEndSync(self.syncResult);
				return deferred.promise;
			}
			if (typeof serverData.data === 'undefined' || serverData.data.length === 0) {
				//nothing to update
				self.db.transaction(function(tx) {
					//We only use the server date to avoid dealing with wrong date from the client
					fnPrivate.finishSync(serverData.syncDate, tx).then(function(){
						deferred.resolve(0);
					});
				});
				return deferred.promise;
			}
			self.db.transaction(function(tx) {
				var counterNbTable = 0,
					counterNbElm = 0,
					nbTables = self.tablesToSync.length;

				self.tablesToSync.forEach(function(table) {
					var currData = serverData.data[table.tableName];
					if (typeof currData === "undefined") {
						//Should always be defined (even if 0 elements)
						currData = [];
					}
					var nb = currData.length;
					counterNbElm += nb;
					fnPrivate.log('There are ' + nb + ' new or modified elements in the table ' + table.tableName + ' to save in the local DB');

					var i = 0, listIdToCheck = [];
					for (i = 0; i < nb; i++) {
						//console.log('IdToCheck: ' + serverData.data[table.tableName][i][table.idName]);	//AB: we passe each id individualy
						//if (serverData.data[table.tableName][i][table.idName] != "") {	//AB: when id = "", it was created in MySQL, do an insert in webSQL
						listIdToCheck.push(serverData.data[table.tableName][i][table.idName]);
						//} //AB:
					}
					//console.log('listIdToCheck: ' +listIdToCheck);	//AB: from ",,,,,6,7" -> "6,7" with the added if(id != "" ou "-1")
					fnPrivate.getIdExitingInDB(table.tableName, table.idName, listIdToCheck, tx).then(function(idInDb) { //AB: "6,7" -> "6,7" OR "-1,-1,-1,-1,-1,6,7" ->? "6,7"

						var curr = null, sql = null;
						for (i = 0; i < nb; i++) {
							curr = serverData.data[table.tableName][i];
							//console.log("tableName: " + table.tableName);	// R:  Contacts
							//console.log("idName   : " + table.idName);	// R:  id or UniteID
							//console.log('idName value: ' + serverData.data[table.tableName][i][table.idName]);	//R: les valeurs de idName. Ex pour Contacts.id: "","","","","","0","2"
							if (idInDb[curr[table.idName]]) { //update
								/*ex : UPDATE "tableName" SET colonne 1 = [valeur 1], colonne 2 = [valeur 2]*/
								sql = fnPrivate.buildUpdateSQL(table.tableName, curr);
								sql += ' WHERE ' + table.idName + ' = "' + curr[table.idName] + '"';
								fnPrivate.executeQuery(sql, []);

							} else { //insert with the id member and its value
								if (serverData.data[table.tableName][i][table.idName] != "") {
									//Ex: "INSERT INTO tablename (id, name, type, ...) VALUES (?, ?, ?, ...)", [params];
									var attList = fnPrivate.getAttributesList(curr);
									sql = fnPrivate.buildInsertSQL(table.tableName, curr, attList);
									var attValues = fnPrivate.getMembersValue(curr, attList);
									fnPrivate.executeQuery(sql, attValues);
								} else { //insert without the id member
									//Ex: "INSERT INTO tablename (name, type, ...) VALUES (?, ?, ...)", [params];	//To let create the id automatically (autoincrement)
									var attList = fnPrivate.getAttributesList(curr);
									//	var attValue = fnPrivate.getMembersValue(curr, attList);
									sql = fnPrivate.buildInsertSQLWithIdNull(table.tableName, curr);
									var attValues = fnPrivate.getMembersValueForIdNull(curr, attList);
									//fnPrivate.executeQuery(sql, []);
									fnPrivate.executeQuery(sql, attValues);
								}
							}
						} //end for

						counterNbTable++;
						if (counterNbTable === nbTables) {
							//TODO set counterNbElm to info
							self.syncResult.nbUpdated = counterNbElm;
							fnPrivate.finishSync(serverData.syncDate, tx).then(function(){
								deferred.resolve();
							});
						}
					}); //end getExisting Id
				}); //end forEach
			}); //end tx
			return deferred.promise;
		}

		/** return the listIdToCheck curated from the id that doesn't exist in tableName and idName
		 * (used in the DBSync class to know if we need to insert new elem or just update)
		 * @param {Object} tableName : card_stat.
		 * @param {Object} idName : ex. card_id.
		 * @param {Object} listIdToCheck : ex. [10000, 10010].
		 */
		function getIdExitingInDB(tableName, idName, listIdToCheck, tx) {
			// listIdToCheck come from ln 298
			var self = DBSYNC, deferred = $q.defer();

			if (listIdToCheck.length === 0) {
				deferred.resolve([]);
				return deferred.promise;
			}
			// SELECT id FROM Contacts WHERE id IN listIdToCheck	// Est-ce utile??? Est-ce que ça crée un bug si le idName n'est pas dans la liste
			var SQL = 'SELECT ' + idName + ' FROM ' + tableName + ' WHERE ' + idName + ' IN ("' + fnPrivate.arrayToString(listIdToCheck, '","') + '")';
			fnPrivate.selectSql(SQL).then(function(ids) {
				var idsInDb = [];
				for (var i = 0; i < ids.length; ++i) {
					idsInDb[ids[i][idName]] = true;
				}
				deferred.resolve(idsInDb);
			});

			return deferred.promise;
		}

		function finishSync(syncDate, tx) {
			var self = DBSYNC, deferred = $q.defer(),
				tableName, idsToDelete, idName, i, idValue, idsString;

			self.firstSync = false;
			self.syncInfo.lastSyncDate = syncDate;
			fnPrivate.executeQuery('UPDATE sync_info SET last_sync = "' + syncDate + '"', []);

			// Remove only the elem sent to the server (in case new_elem has been added during the sync)
			// We don't do that anymore: self.executeQuery('DELETE FROM new_elem', []);
			for (tableName in self.clientData.data) {
				idsToDelete = [];
				idName = self.idNameFromTableName[tableName];
				for (i = 0; i < self.clientData.data[tableName].length; i++) {
					idValue = self.clientData.data[tableName][i][idName];
					idsToDelete.push('"' + idValue + '"');
				}
				if (idsToDelete.length > 0) {
					idsString = fnPrivate.arrayToString(idsToDelete, ',');
					fnPrivate.executeQuery('DELETE FROM new_elem WHERE table_name = "' + tableName + '" AND id IN (' + idsString + ')', []);
				}
			}
			// Remove elems received from the server that has triggered the SQL TRIGGERS, to avoid to send it again to the server and create a loop
			for (tableName in self.serverData.data) {
				idsToDelete = [];
				idName = self.idNameFromTableName[tableName];
				for (i = 0; i < self.serverData.data[tableName].length; i++) {
					idValue = self.serverData.data[tableName][i][idName];
					idsToDelete.push('"' + idValue + '"');
				}
				if (idsToDelete.length > 0) {
					idsString = fnPrivate.arrayToString(idsToDelete, ',');
					fnPrivate.executeQuery('DELETE FROM new_elem WHERE table_name = "' + tableName + '" AND id IN (' + idsString + ')', []);
					fnPrivate.executeQuery('DELETE FROM ' + tableName + ' WHERE ' + idName + ' NOT IN (' + idsString + ')', []);
				}
			}

			deferred.resolve();
			self.clientData = null;
			self.serverData = null;

			return deferred.promise;
		}

		/***************** DB  util ****************/

		function whereClause(b) {
			var a = "",
				v = [];
			for (var c in b) {
				if (typeof b[c] !== "undefined" && typeof b[c] !== "object" && typeof b[c] === "string" && !b[c].match(/NULL/ig)) v.push(b[c]);
				else if (typeof b[c] !== "undefined" && typeof b[c] !== "object" && typeof b[c] === "number") v.push(b[c]);
				else if (typeof b[c]["value"] !== "undefined" && typeof b[c] === "object" && (typeof b[c]["value"] === "number" || !b[c]["value"].match(/NULL/ig))) v.push(b[c]["value"]);
				a += (typeof b[c] === "object") ?
					(typeof b[c]["union"] === "undefined") ?
					(typeof b[c]["value"] === "string" && b[c]["value"].match(/NULL/ig)) ?
					"`" + c + "` " + b[c]["value"] :
					(typeof b[c]["operator"] !== "undefined") ?
					"`" + c + "` " + b[c]["operator"] + " ? " :
					"`" + c + "` = ?" :
					(typeof b[c]["value"] === "string" && b[c]["value"].match(/NULL/ig)) ?
					"`" + c + "` " + b[c]["value"] + " " + b[c]["union"] + " " :
					(typeof b[c]["operator"] !== "undefined") ?
					"`" + c + "` " + b[c]["operator"] + " ? " + b[c]["union"] + " " :
					"`" + c + "` = ? " + b[c]["union"] + " " :
					(typeof b[c] === "string" && b[c].match(/NULL/ig)) ?
					"`" + c + "` " + b[c] :
					"`" + c + "` = ?"
			}
			return {
				w: a,
				p: v
			};
		}

		function executeQuery(query, values) {
			fnPrivate.log('# Execute Sql: ' + query + ' with param ' + values);

			var deferred = $q.defer();
			DBSYNC.db.transaction(function(tx) {
				tx.executeSql(query, values, function(tx, results) {
					deferred.resolve(results);
				}, function(tx, e) {
					fnPrivate.errorHandler(tx, e);
					deferred.reject();
				});
			});
			return deferred.promise;
		}

		function replace(a, c) {
			for (var b in c) {
				a = a.replace(new RegExp(b, "ig"), c[b])
			}
			return a;
		}

		function selectSql(sql) {
			var deferred = $q.defer();

			fnPrivate.executeQuery(sql, []).then(function(result) {
				deferred.resolve(fnPrivate.transformRs(result));
			}, fnPrivate.errorHandler);

			return deferred.promise;
		}

		function transformRs(rs) {
			var elms = [];
			if (typeof(rs.rows) === 'undefined') {
				return elms;
			}

			for (var i = 0; i < rs.rows.length; ++i) {
				elms.push(rs.rows.item(i));
			}
			return elms;
		}

		function errorHandler(transaction, error) {
			fnPrivate.error('Error : ' + error.message + ' (Code ' + error.code + ') Transaction.sql = ' + transaction.sql);
		}

		function buildInsertSQL(tableName, objToInsert) {
			var members = fnPrivate.getAttributesList(objToInsert);
			if (members.length === 0) {
				throw 'buildInsertSQL : Error, try to insert an empty object in the table ' + tableName;
			}
			//build INSERT INTO myTable (attName1, attName2) VALUES (?, ?) -> need to pass the values in parameters
			var sql = 'INSERT INTO ' + tableName + ' (';
			sql += fnPrivate.arrayToString(members, ',');
			sql += ') VALUES (';
			sql += fnPrivate.getNbValString(members.length, '?', ',');
			sql += ')';

			//console.log("members i=1:" + members[1]); // members[1] = id if the table is Contacts
			//	var values = fnPrivate.getMembersValue(objToInsert, members); serverData.data[table.tableName][i][table.idName]);
			//	console.log("values i=1:" + values[1]); // values[1] = "" if the table is Contacts and the contact was created in MySQL
			return sql;
		}

		function buildInsertSQLWithIdNull(tableName, objToInsert) { // where objToInsert = curr = serverData.data[table.tableName][i]
			//   INSERT INTO Contacts (ContactID, firstName, lastName, qte, MaJdate, cbFait, rbABC, UniteId) VALUES (?, ?, ?, ?, ?, ?, ?, ?);", ['1', 'Alain', 'Alarie', '1', '2013-01-01', '1', 'A', '1']
			//mais j'ai: INSERT INTO Contacts (ContactID, firstName, lastName, qte, MaJdate, cbFait, rbABC, UniteID, last_sync_date) VALUES (7,Zébulon,Zala,2.20,2013-09-23,0,C,2,2013-10-02 08:21:44)
			var members = fnPrivate.getAttributesList(objToInsert);
			//console.log('members.length: ' + members.length);	//R: 10 records -> Good
			if (members.length === 0) {
				throw 'buildInsertSQLWithIdNull : Error, try to insert an empty object in the table ' + tableName;
			}

			/*        //build INSERT INTO myTable (attName1, attName2) VALUES (?, ?) -> need to pass the values in parameters
			 */
			//AB: if id=""(a record created in MySQL) we should remove the id field from the query to allow webSQL to do its autoincrement
			var values = fnPrivate.getMembersValue(objToInsert, members); // bug if we use self. instead of using this.
			var sqlWithIdNull = 'INSERT INTO ' + tableName + ' (';
			var sqlMembers = '';
			var sqlQmark = '';
			//var valuesArray = [];
			//var sqlCommas = '';
			//var sqlValues = '';
			var nb = members.length;
			for (var i = 0; i < nb; i++) {
				//console.log('member i: ' + members[i]);	// R: Give the colomn name of the table Ex: ContactID, id, firstName, lastName, ...
				//console.log('value  i: ' + values[i]);	// R: Give the value for each colomn of each record Ex: 3, "", Victor, Villeneuve, ...
				if (members[i] != 'id') { //if id, we dont include the the id in the INSERT query to force webSQL to do its autoincrement on id
					sqlMembers += members[i];
					sqlQmark += '?';
					//valuesArray.push(values[i]);
					//sqlValues  += values[i];
					if (i < nb - 1) {
						sqlMembers += ',';
						sqlQmark += ',';
						//sqlValues  += ',';
					}
				}
			}
			sqlWithIdNull += sqlMembers + ') VALUES(' + sqlQmark + ')';
			fnPrivate.log(sqlWithIdNull);
			return sqlWithIdNull;
		}

		function getMembersValueForIdNull(obj, members) {
			var valuesArray = [];
			for (var i = 0; i < members.length; i++) {
				if (members[i] != 'id') { //if id, we dont include the value in the INSERT query to force webSQL to do its autoincrement on id
					valuesArray.push(obj[members[i]]);
				}
			}
			return valuesArray;
		}

		function buildUpdateSQL(tableName, objToUpdate) {
			/*ex UPDATE "nom de table" SET colonne 1 = [valeur 1], colonne 2 = [valeur 2] WHERE {condition}*/
			var self = DBSYNC;
			var sql = 'UPDATE ' + tableName + ' SET ';
			var members = fnPrivate.getAttributesList(objToUpdate);
			if (members.length === 0) {
				throw 'buildUpdateSQL : Error, try to insert an empty object in the table ' + tableName;
			}
			var values = fnPrivate.getMembersValue(objToUpdate, members);

			var nb = members.length;
			for (var i = 0; i < nb; i++) {
				sql += '"' + members[i] + '" = "' + values[i] + '"';
				if (i < nb - 1) {
					sql += ', ';
				}
			}

			return sql;
		}

		function getMembersValue(obj, members) {
			var memberArray = [];
			for (var i = 0; i < members.length; i++) {
				memberArray.push(obj[members[i]]);
			}
			return memberArray;
		}

		function getAttributesList(obj, check) {
			var memberArray = [];
			for (var elm in obj) {
				if (check && typeof this[elm] === 'function' && !obj.hasOwnProperty(elm)) {
					continue;
				}
				memberArray.push(elm);
			}
			return memberArray;
		}

		function getNbValString(nb, val, separator) {
			var result = '';
			for (var i = 0; i < nb; i++) {
				result += val;
				if (i < nb - 1) {
					result += separator;
				}
			}
			return result;
		}

		function getMembersValueString(obj, members, separator) {
			var result = '';
			for (var i = 0; i < members.length; i++) {
				result += '"' + obj[members[i]] + '"';
				if (i < members.length - 1) {
					result += separator;
				}
			}
			return result;
		}

		function arrayToString(array, separator) {
			var result = '';
			for (var i = 0; i < array.length; i++) {
				result += array[i];
				if (i < array.length - 1) {
					result += separator;
				}
			}
			return result;
		}

	}

})();