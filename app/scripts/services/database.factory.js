(function() {

	'use strict';

	angular.module('AppVizcaya.services')
		.factory('$database', database);

	database.$inject = ['DB_CONFIG', '$webSyncSQL', '$http', '$q', '$rootScope'];

	function database(DB_CONFIG, $webSyncSQL, $http, $q, $rootScope) {

		var self = this;

		self = {
			syncNow  : syncNow,
			initSync : initSync
		};

		return self;

		///////////////////////////////////////////


		function syncNow() {
			$webSyncSQL.openDB(DB_CONFIG.dbName, '1.0', 'local database for app Vizcaya', 10 * 1024 * 1024);

			// loading
			$rootScope.isLoading = true;

			// verificar se tabelas exitem pra poder dar droptable
			// resetDB();
			createTables();


			// $webSyncSQL.dropTable('sync_info');
			// $webSyncSQL.dropTable('new_elem');
		}

		function initSync() {
			$webSyncSQL.initSync(DB_CONFIG.tablesToSync, DB_CONFIG.sync_info, DB_CONFIG.sync_URL, DB_CONFIG.isDebug).then(callBackInitSync);

			function callBackInitSync (data) {

				// passar true atualiza somente banco local
				$webSyncSQL.syncNow(false).then(function(result){
					$rootScope.isLoading = false;

					navigator.notification.alert(
	                    'Dados sincronizados com sucesso.',  // message
	                    null,           // callback
	                    'Atenção',      // title
	                    'OK'            // buttonName
	                );
				}, function(error){
					$rootScope.isLoading = false;

					navigator.notification.alert(
	                    'Não foi possível sincronizar. Falha ao conectar com a internet.',  // message
	                    null,           // callback
	                    'Atenção',      // title
	                    'OK'            // buttonName
	                );
				}, function(progress){
					// console.log(progress);
				});
			}
		}

		function resetDB() {
			var prom = [];

			// realiza um droptable em todas as tabelas
			angular.forEach(DB_CONFIG.tables, function(table) {
				prom.push( $webSyncSQL.dropTable(table.name) );
			});

			// cria, se ainda não existirem, todas as tabelas
	        $q.all(prom).then(function(){
	        	// createTables();
	        });
		}

		function createTables() {
			var prom = [];

			// cria, se ainda não existirem, todas as tabelas
			angular.forEach(DB_CONFIG.tables, function(table) {
				prom.push( $webSyncSQL.createTable(table.name, table.columns) );
			});

			// inicia a sincronização
			$q.all(prom).then( function(){
				initSync();
			});
		}

	}

})();