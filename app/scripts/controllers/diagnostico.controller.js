(function(){

    'use strict';

    angular.module('AppVizcaya.controllers')
        .controller('diagnosticoCtrl', diagnosticoCtrl);

    diagnosticoCtrl.$inject = ['$webSyncSQL', '$rootScope'];

    function diagnosticoCtrl($webSyncSQL, $rootScope) {

        var vm = this;

        vm.diagnosticoSelected = {
            tipo: [{
                nome: 'Normal',
                isSelected: 0
            },
            {
                nome: 'Oleoso',
                isSelected: 0
            },
            {
                nome: 'Misto',
                isSelected: 0
            },
            {
                nome: 'Maduros',
                isSelected: 0
            }],
            forma : [{
                nome: 'Liso',
                isSelected: 0
            },
            {
                nome: 'Ondulado',
                isSelected: 0
            },
            {
                nome: 'Cacheado',
                isSelected: 0
            },
            {
                nome: 'Crespo',
                isSelected: 0
            }],
            cor : [{
                nome: 'Preto',
                isSelected: 0
            },
            {
                nome: 'Castanho',
                isSelected: 0
            },
            {
                nome: 'Loiro',
                isSelected: 0
            },
            {
                nome: 'Branco',
                isSelected: 0
            },
            {
                nome: 'Ruivo',
                isSelected: 0
            }],
            problema: {
                ressecados  : 0,
                quebradicos : 0,
                danificados : 0,
                queda       : 0,
                frizz       : 0,
                volume      : 0,
                finos       : 0,
                residuos    : 0
            },
            tratamento: {
                alisamento   : 0,
                coloracao    : 0,
                descoloracao : 0,
                nenhum       : 0
            }
        };

        vm.setDefaultValues = setDefaultValues;
        vm.verifyLength = verifyLength;
        vm.update = update;

        ///////////////////////


        function setDefaultValues(array, value) {
            angular.forEach(array, function (value) {
                value.isSelected = 0;
            });

            value.isSelected = 1;
        };

        // checks the checkbox number is greater than limit
        function verifyLength(max, name) {
            var inputs = angular.element(document.querySelectorAll("input[name="+name+"]")),
                inputsChecked = angular.element(document.querySelectorAll("input[name="+name+"]:checked"));

            if( inputsChecked.length == max ){
                inputs.attr('disabled', 'disabled');
                inputsChecked.removeAttr('disabled');
            } else {
                inputs.removeAttr('disabled');
                vm.disablebtn = false;
            }

            if (angular.element(document.querySelectorAll("input[name='problema']:checked")).length != 0 &&
                angular.element(document.querySelectorAll("input[name='tratamento']:checked")).length != 0) {
                vm.disablebtn = true;
            }
        }

        function update() {

            $webSyncSQL.select('clientes', {
                'email': {
                    'value': vm.emailCliente
                }
            }).then(function(results) {

                if (results.rows.length == 1){
                    $webSyncSQL.update('clientes',{
                        consultorID    : $rootScope.adminID || 0,
                        t1normal       : vm.diagnosticoSelected.tipo[0].isSelected,
                        t1oleoso       : vm.diagnosticoSelected.tipo[1].isSelected,
                        t1misto        : vm.diagnosticoSelected.tipo[2].isSelected,
                        t1maduros      : vm.diagnosticoSelected.tipo[3].isSelected,
                        t2liso         : vm.diagnosticoSelected.forma[0].isSelected,
                        t2ondulado     : vm.diagnosticoSelected.forma[1].isSelected,
                        t2cacheado     : vm.diagnosticoSelected.forma[2].isSelected,
                        t2crespo       : vm.diagnosticoSelected.forma[3].isSelected,
                        t3preto        : vm.diagnosticoSelected.cor[0].isSelected,
                        t3castanho     : vm.diagnosticoSelected.cor[1].isSelected,
                        t3loiro        : vm.diagnosticoSelected.cor[2].isSelected,
                        t3branco       : vm.diagnosticoSelected.cor[3].isSelected,
                        t3ruivo        : vm.diagnosticoSelected.cor[4].isSelected,
                        t4ressecados   : vm.diagnosticoSelected.problema.ressecados,
                        t4quebradicos  : vm.diagnosticoSelected.problema.quebradicos,
                        t4danificados  : vm.diagnosticoSelected.problema.danificados,
                        t4queda        : vm.diagnosticoSelected.problema.queda,
                        t4frizz        : vm.diagnosticoSelected.problema.frizz,
                        t4volume       : vm.diagnosticoSelected.problema.volume,
                        t4finos        : vm.diagnosticoSelected.problema.finos,
                        t4residuos     : vm.diagnosticoSelected.problema.residuos,
                        t5alisamento   : vm.diagnosticoSelected.tratamento.alisamento,
                        t5nenhum       : vm.diagnosticoSelected.tratamento.nenhum,
                        t5coloracao    : vm.diagnosticoSelected.tratamento.coloracao,
                        t5descoloracao : vm.diagnosticoSelected.tratamento.descoloracao
                    }, {email: vm.emailCliente}).then(function(results) {

                        navigator.notification.alert(
                            'Diagnóstico efetuado com sucesso.',  // message
                            null,           // callback
                            'Atenção',      // title
                            'OK'            // buttonName
                        );

                        location.reload();

                    }, function(error){
                        navigator.notification.alert(
                            'Erro ao tentar salvar diagnóstico.',  // message
                            null,           // callback
                            'Atenção',      // title
                            'OK'            // buttonName
                        );
                    });
                }
                else {
                    navigator.notification.alert(
                        'E-mail não cadastrado',  // message
                        null,           // callback
                        'Atenção',      // title
                        'OK'            // buttonName
                    );
                }
            });
        }

    }

})();