(function() {

    'use strict';

    angular.module('AppVizcaya.controllers')
        .controller('cadastroUserCtrl', cadastroUserCtrl);

    cadastroUserCtrl.$inject = ['$webSyncSQL', '$rootScope', '$scope'];

    function cadastroUserCtrl($webSyncSQL, $rootScope, $scope) {

        var vm = this, img64;

        vm.newsletter  = 0;
        vm.imgprofile  = false;
        vm.takePicture = takePicture;
        vm.save        = save;

        ///////////////////////


        function takePicture() {

            var cameraOptions = {
                quality          : 75,
                destinationType  : Camera.DestinationType.DATA_URL,
                sourceType       : Camera.PictureSourceType.CAMERA,
                encodingType     : Camera.EncodingType.JPEG,
                targetWidth      : 640,
                targetHeight     : 640,
                saveToPhotoAlbum : false
            };

            navigator.camera.getPicture( cameraSuccess, cameraError, cameraOptions );

            function cameraSuccess(imageData) {
                var image = document.getElementById('profilePhoto');

                $scope.$apply(function(){
                    vm.imgprofile = true;
                    img64         = imageData;
                    image.src     = 'data:image/jpeg;base64,' + imageData;
                });
            }

            function cameraError(data) {
                navigator.notification.alert(
                    'Erro ao salvar imagem',  // message
                    null,           // callback
                    'Atenção',      // title
                    'OK'            // buttonName
                );
            }
        }

        function save() {

            $webSyncSQL.insert('clientes', {
                name           : vm.username,
                email          : vm.email,
                img            : 'data:image/jpeg;base64,' + img64,
                created        : Date.now(),
                consultorID    : $rootScope.adminID || 0,
                t1normal       : 0,
                t1oleoso       : 0,
                t1misto        : 0,
                t1maduros      : 0,
                t2liso         : 0,
                t2ondulado     : 0,
                t2cacheado     : 0,
                t2crespo       : 0,
                t3preto        : 0,
                t3castanho     : 0,
                t3loiro        : 0,
                t3branco       : 0,
                t3ruivo        : 0,
                t4ressecados   : 0,
                t4quebradicos  : 0,
                t4danificados  : 0,
                t4queda        : 0,
                t4frizz        : 0,
                t4volume       : 0,
                t4finos        : 0,
                t4residuos     : 0,
                t5alisamento   : 0,
                t5nenhum       : 0,
                t5coloracao    : 0,
                t5descoloracao : 0,
                newsletter     : vm.newsletter,
                last_sync_date : ''
            }).then(function(results) {

                navigator.notification.alert(
                    'Cliente salvo com sucesso.',  // message
                    null,           // callback
                    'Atenção',      // title
                    'OK'            // buttonName
                );

                location.reload();

            }, function(error){
                navigator.notification.alert(
                    'Erro ao tentar salvar cliente.',  // message
                    null,           // callback
                    'Atenção',      // title
                    'OK'            // buttonName
                );
            });
        }

    }

})();