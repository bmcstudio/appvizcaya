(function(){

    'use strict';

    angular.module('AppVizcaya.controllers')
        .controller('cadastroVendaCtrl', cadastroVendaCtrl);

    cadastroVendaCtrl.$inject = ['$webSyncSQL', '$scope'];

    function cadastroVendaCtrl($webSyncSQL, $scope) {

        var vm = this;

        vm.gallery = [
            { image: 'images/no-image.svg' },
            { image: 'images/no-image.svg' },
            { image: 'images/no-image.svg' },
            { image: 'images/no-image.svg' },
            { image: 'images/no-image.svg' },
            { image: 'images/no-image.svg' }
        ];

        vm.save = save;
        vm.takePicture = takePicture;
        vm.removePicture = removePicture;

        ///////////////////////


        function save() {

            $webSyncSQL.insert('pdv', {
                nome           : vm.nameplace,
                endereco       : vm.address,
                created        : Date.now(),
                img0           : (vm.gallery[0].image != 'images/no-image.svg') ? vm.gallery[0].image : '',
                img1           : (vm.gallery[1].image != 'images/no-image.svg') ? vm.gallery[1].image : '',
                img2           : (vm.gallery[2].image != 'images/no-image.svg') ? vm.gallery[2].image : '',
                img3           : (vm.gallery[3].image != 'images/no-image.svg') ? vm.gallery[3].image : '',
                img4           : (vm.gallery[4].image != 'images/no-image.svg') ? vm.gallery[4].image : '',
                img5           : (vm.gallery[5].image != 'images/no-image.svg') ? vm.gallery[5].image : '',
                last_sync_date : ''
            }).then(function(results) {

                navigator.notification.alert(
                    'Ponto de venda salvo com sucesso.',  // message
                    null,           // callback
                    'Atenção',      // title
                    'OK'            // buttonName
                );

                location.reload();

            }, function(error){
                navigator.notification.alert(
                    'Erro ao tentar salvar ponto de venda.',  // message
                    null,           // callback
                    'Atenção',      // title
                    'OK'            // buttonName
                );
            });
        }

        function takePicture(item) {

            var cameraOptions = {
                quality          : 75,
                destinationType  : Camera.DestinationType.DATA_URL,
                sourceType       : Camera.PictureSourceType.CAMERA,
                encodingType     : Camera.EncodingType.JPEG,
                targetWidth      : 640,
                targetHeight     : 640,
                saveToPhotoAlbum : false
            };

            navigator.camera.getPicture( cameraSuccess, cameraError, cameraOptions );

            function cameraSuccess(imageData) {
                $scope.$apply(function(){
                    item.image = 'data:image/jpeg;base64,' + imageData;
                });
            }

            function cameraError(data) {
                navigator.notification.alert(
                    'Erro ao salvar imagem',  // message
                    null,           // callback
                    'Atenção',      // title
                    'OK'            // buttonName
                );
            }
        }

        function removePicture(item) {
            item.image = 'images/no-image.svg';
        }

    }

})();