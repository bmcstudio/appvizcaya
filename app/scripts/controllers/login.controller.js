(function(){

	'use strict';

	angular.module('AppVizcaya.controllers')
		.controller('loginCtrl', loginCtrl);

	loginCtrl.$inject = ['$webSyncSQL', '$state', '$rootScope'];

	function loginCtrl($webSyncSQL, $state, $rootScope) {

		var vm = this;

		vm.login = login;

		///////////////////////


		function login() {
			$webSyncSQL.select('admin_user', {
				'email': {
					'value': vm.user.email,
					'union': 'AND'
				},
				'password': {
					'value': vm.user.password
				}
			}).then(function(results) {
				// console.log(results);
				// console.log(results.rows);
				// console.log(results.rows.item(0));

				if (results.rows.length == 1){
					$rootScope.adminID = results.rows.item(0).adminID;
					$state.go('app.home');
				}
				else {
					navigator.notification.alert(
						'E-mail ou senha incorretos.',  // message
						null,         	// callback
						'Atenção',    	// title
						'OK'          	// buttonName
					);
				}
			});
		}

	}

})();