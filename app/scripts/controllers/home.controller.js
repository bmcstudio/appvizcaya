(function() {

	'use strict';

	angular.module('AppVizcaya.controllers')
		.controller('homeCtrl', homeCtrl);

	// homeCtrl.$inject = [''];

	function homeCtrl() {

		var vm = this;

		vm.openPDF = openPDF;

		///////////////////////


		function openPDF() {
			window.open('http://bmc.ppg.br/bardez/Vizcaya/pdf/viz.pdf', '_system', 'location=no');
		}

	}

})();